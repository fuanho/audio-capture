import sys
import os
import threading
import atexit
import time
import numpy as np

import pyaudio
import parselmouth
from PyQt5 import QtWidgets, QtMultimedia, QtMultimediaWidgets, QtCore
from PyQt5.QtWidgets import QFileDialog, QLayoutItem
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT
from matplotlib.figure import Figure

from ui_main import Ui_MainWindow


class MicrophoneRecorder(object):
    def __init__(self, rate=8000, chunksize=1024):
        self.rate = rate
        self.chunksize = chunksize
        self.p = pyaudio.PyAudio()
        self.stream = self.p.open(format=pyaudio.paInt16,
                                  channels=1,
                                  rate=self.rate,
                                  input=True,
                                  frames_per_buffer=self.chunksize,
                                  stream_callback=self.new_frame)
        self.lock = threading.Lock()
        self.stop = False
        self.frames = []
        atexit.register(self.close)

    def new_frame(self, data, frame_count, time_info, status):
        data = np.fromstring(data, 'int16')
        with self.lock:
            self.frames.append(data)
            if self.stop:
                return None, pyaudio.paComplete
        return None, pyaudio.paContinue

    def get_frames(self):
        with self.lock:
            frames = self.frames
            self.frames = []
            return frames

    def start(self):
        self.stream.start_stream()

    def close(self):
        with self.lock:
            self.stop = True
        self.stream.close()
        self.p.terminate()


class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)


class Camera(QtMultimediaWidgets.QCameraViewfinder):
    def __init__(self, i):
        self.webcams = QtMultimedia.QCameraInfo.availableCameras()
        if not self.webcams:
            msg_box = QtWidgets.QMessageBox()
            msg_box.setWindowTitle("Error")
            msg_box.setText("No camera plugin")
            msg_box.exec_()
        self.exist = QtMultimediaWidgets.QCameraViewfinder()
        self.get_webcam(i)
        super(Camera, self).__init__()

    def get_webcam(self, i):
        self.my_webcam = QtMultimedia.QCamera(self.webcams[i])
        self.my_webcam.setViewfinder(self.exist)
        self.my_webcam.setCaptureMode(QtMultimedia.QCamera.CaptureStillImage)
        self.my_webcam.error.connect(lambda: self.alert(self.my_webcam.errorString()))
        self.my_webcam.start()

        self.capture = QtMultimedia.QCameraImageCapture(self.my_webcam)
        self.capture.error.connect(lambda error_msg, error, msg: self.alert(msg))
        self.capture.imageCaptured.connect(
            lambda d, i: self.status.showMessage("Image captured : " + str(self.save_seq)))
        self.current_camera_name = self.webcams[i].description()
        self.save_seq = 0

    def save_dir(self):
        path = QFileDialog.getExistingDirectory(self, "Picture Location", "")

        if path:
            self.save_path = path
            self.save_seq = 0

    def capture_img(self, path):
        timestamp = time.strftime("%d-%b-%Y-%H_%M_%S")

        self.capture.capture(os.path.join(self.save_path, "%s-%04d-%s.jpg" % (
            self.current_camera_name,
            self.save_seq,
            timestamp
        )))

        # increment the sequence
        self.save_seq += 1

    def alert(self, s):
        err = QtWidgets.QErrorMessage(self)
        err.showMessage(s)


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.file_name = ""
        self.action()

    def action(self):
        self.ui.actionOpen_File.triggered.connect(self.open_file_dialog)
        self.ui.action_save_dir.triggered.connect(Camera.save_dir)
        self.ui.action_capture.triggered.connect(Camera.capture_img)
        self.ui.showHistogram.clicked.connect(self.draw)
        self.ui.web_cam.clicked.connect(self.webcam)
        self.ui.audio.clicked.connect(self.audio)

    def open_file_dialog(self):
        path = QFileDialog.getOpenFileNames(self, "Open file", "/home", "*.wav(*.wav)")

        if path[0]:
            self.file_name = path[0][0]

    def clear_vbox(self, layout=False):
        if not layout:
            layout = self.ui.verticalLayout
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.deleteLater()
                else:
                    self.clear_vbox(item.layout())

    def draw(self):
        self.timer.stop()
        self.clear_vbox(self.ui.verticalLayout)

        # snd = parselmouth.Sound(self.file_name)
        # w = QtWidgets.QVBoxLayout()

        # self.clear_vbox(self.ui.verticalLayout)
        # canvas = MplCanvas(self, width=5, height=4, dpi=100)
        # canvas.axes.plot(snd.xs(), snd.values.T)
        # canvas.axes.set_xlabel("time [s]")
        # canvas.axes.set_ylabel("amplitude")
        # toolbar = NavigationToolbar2QT(canvas, self)
        # w.addWidget(toolbar)
        # w.addWidget(canvas)
        # self.ui.verticalLayout.addLayout(w)

    def webcam(self):
        cam = Camera(0)
        self.cam = cam
        self.ui.verticalLayout.addWidget(cam.exist)

    def audio(self):
        mic = MicrophoneRecorder()
        mic.start()
        self.mic = mic
        self.freq_vect = np.fft.rfftfreq(mic.chunksize, 1. / mic.rate)
        self.time_vect = np.arange(mic.chunksize, dtype=np.float32) / mic.rate * 1000

        self.audio_plot_up = MplCanvas(self, width=5, height=4, dpi=100)
        self.audio_plot_up.axes.set_ylim(-32768, 32768)
        self.audio_plot_up.axes.set_xlim(0, self.time_vect.max())
        self.audio_plot_up.axes.set_xlabel(u"time (ms)", fontsize=6)
        self.line_top, = self.audio_plot_up.axes.plot(self.time_vect, np.ones_like(self.time_vect))

        self.update_audio_plot()

        self.ui.verticalLayout.addWidget(self.audio_plot_up)

        self.timer = QtCore.QTimer()
        self.timer.setInterval(10)
        self.timer.timeout.connect(self.update_audio_plot)
        self.timer.start()

    def update_audio_plot(self):
        frames = self.mic.get_frames()

        if len(frames) > 0:
            current_frame = frames[-1]
            self.line_top.set_data(self.time_vect, current_frame)
            self.audio_plot_up.draw()


if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
